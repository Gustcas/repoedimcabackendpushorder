The Back End is made with the Spring Tool Suite 4 maven and the Spring Starter Project version 2.5.4 was used with Java 8 and JRE 1.8


The dependencies used were:

- Spring Web
- Spring JPA
- Spring Web Service
- JDBC API
- Lombok
- PostgreSql Driver
- h2database


Getting started

The project must first be cloned or downloaded, and then imported into the STS.

Second, you must validate that all your dependencies run

Third, it must be validated that the JRE 1.8 is on the property of the project

Note: the project must be mounted on the same version of STS that was used for it