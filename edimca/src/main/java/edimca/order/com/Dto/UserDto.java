package edimca.order.com.Dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class UserDto {
   private String username;
   private String password;
   private boolean logged;
   private String message;
}
