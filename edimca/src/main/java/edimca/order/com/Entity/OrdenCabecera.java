package edimca.order.com.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name ="ordencabecera")
public class OrdenCabecera {
	
	public int getIdorden() {
		return idorden;
	}

	public void setIdorden(int idorden) {
		this.idorden = idorden;
	}

	public String getFechacreacionorden() {
		return fechacreacionorden;
	}

	public void setFechacreacionorden(String fechacreacionorden) {
		this.fechacreacionorden = fechacreacionorden;
	}

	public String getNombrecliente() {
		return nombrecliente;
	}

	public void setNombrecliente(String nombrecliente) {
		this.nombrecliente = nombrecliente;
	}

	public String getCedulacliente() {
		return cedulacliente;
	}

	public void setCedulacliente(String cedulacliente) {
		this.cedulacliente = cedulacliente;
	}

	public String getDireccioncliente() {
		return direccioncliente;
	}

	public void setDireccioncliente(String direccioncliente) {
		this.direccioncliente = direccioncliente;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	@Id
    @Column(name = "idorden")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idorden;
	
	@Column(name = "fechacreacionorden")
	private String fechacreacionorden;	

	@Column(name = "nombrecliente")
	private String nombrecliente;
	
	@Column(name = "cedulacliente")
	private String cedulacliente;
	
	@Column(name = "direccioncliente")
	private String direccioncliente;
	
	@Column(name = "estado")
	private char estado;
	 
	
}
