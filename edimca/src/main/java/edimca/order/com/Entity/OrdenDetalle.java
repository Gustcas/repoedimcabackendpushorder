package edimca.order.com.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name ="ordendetalle")
public class OrdenDetalle {	

	public int getIdordendetalle() {
		return idordendetalle;
	}

	public void setIdordendetalle(int idordendetalle) {
		this.idordendetalle = idordendetalle;
	}

	public int getIdproducto() {
		return idproducto;
	}

	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}

	public int getIdorden() {
		return idorden;
	}

	public void setIdorden(int idorden) {
		this.idorden = idorden;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	@Id
    @Column(name = "idordendetalle")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idordendetalle;
	
    @Column(name = "idproducto")
    private int idproducto;
    
    @Column(name = "idorden")
    private int idorden;
    
    
    @Column(name = "cantidad")
    private int cantidad;
    
    @Column(name = "iva")
    private double iva;
    
    @Column(name = "total")
    private double total; 

    @Column(name = "estado")
    private char estado;    
 
}
