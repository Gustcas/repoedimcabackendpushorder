package edimca.order.com.Entity;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name ="producto")
public class Producto  implements Serializable {
	
	public int getIdproducto() {
		return idproducto;
	}

	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}

	public String getNombreproducto() {
		return nombreproducto;
	}

	public void setNombreproducto(String nombreproducto) {
		this.nombreproducto = nombreproducto;
	}

	public Double getPrecioventa() {
		return precioventa;
	}

	public void setPrecioventa(Double precioventa) {
		this.precioventa = precioventa;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

    private static final long serialVersionUID = 1L;    
	@Id
    @Column(name = "idproducto")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idproducto;
	
    @Column(name = "nombreproducto")
    private String nombreproducto;
    
    @Column(name = "precioventa")
    private Double precioventa;
    
    @Column(name = "stock")
    private int stock;
    
    @Column(name = "estado")
    private char estado;
}
