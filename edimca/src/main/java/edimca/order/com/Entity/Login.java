package edimca.order.com.Entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
@Data
@Entity
@Table(name ="login")
public class Login implements Serializable {
    private static final long serialVersionUID = 1L;    
    public int getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(int idusuario) {
		this.idusuario = idusuario;
	}

	public String getNombreusuario() {
		return nombreusuario;
	}

	public void setNombreusuario(String nombreusuario) {
		this.nombreusuario = nombreusuario;
	}

	public String getApellidousuario() {
		return apellidousuario;
	}

	public void setApellidousuario(String apellidousuario) {
		this.apellidousuario = apellidousuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Id
    @Column(name = "idusuario")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idusuario;
    
    @Column(name = "nombreusuario")
    private String nombreusuario;
    
    @Column(name = "apellidousuario")
    private String apellidousuario;
    
    @Column(name = "usuario")
    private String usuario;
    
    @Column(name = "password")
    private String password;    
    
    @Column(name = "estado")
    private char estado;
}
