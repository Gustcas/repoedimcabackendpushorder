package edimca.order.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdimcaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdimcaApplication.class, args);
	}

}
