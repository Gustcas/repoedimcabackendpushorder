package edimca.order.com.Servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edimca.order.com.Dao.LoginDao;
import edimca.order.com.Dto.UserDto;
import edimca.order.com.Entity.Login;

@Service
public class LoginServicio {

	@Autowired
	private LoginDao _loginDao;

	/*
	 * Metodo basico para validar el Login Parametros de entrada ( String Usuario,
	 * String Password)
	 * 
	 */
	public Boolean consultaUsuario(String usuario,String password)    {
		boolean isLogin = false;
		List<Login> _lstRespuesta = _loginDao.isValidoLogin(usuario, password);
		if (_lstRespuesta.size() > 0) {
			isLogin = true;
		}
		return isLogin;
	}
	
	public UserDto loginUserDto(String usuario,String password)    {
		Login _lstRespuesta = _loginDao.isValidoLoginUserPass(usuario, password);		
		UserDto _uerDto = new UserDto();
		try {
			if(_lstRespuesta!=null) {
				_uerDto.setUsername(_lstRespuesta.getUsuario());
				_uerDto.setLogged(true);
				_uerDto.setMessage("Login exitoso");
			}else {
				_uerDto.setLogged(false);	
				_uerDto.setMessage("Usuario/password incorrecto");
			}			
		} catch (Exception ex) {
			// TODO: handle exception
			_uerDto.setLogged(false);	
			_uerDto.setMessage(ex.getMessage());
		}	
		return _uerDto;
	}
	

}

