package edimca.order.com.Servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edimca.order.com.Dao.OrdenCabeceraDao;
import edimca.order.com.Dao.OrdenDetalleDao;

import edimca.order.com.Dto.OrdenDetalleDtoDao;
import edimca.order.com.Entity.OrdenCabecera;
import edimca.order.com.Entity.OrdenDetalle;
@Service
public class OrdenServicio {
	@Autowired
	private OrdenCabeceraDao _ordenCabeceraDao;
	@Autowired
	private OrdenDetalleDao _ordenDetalleDao;
	
	public OrdenCabecera guardarOrden(OrdenCabecera _ordenCabecera) {
		System.out.println("Total registros eliminados en batch ************************ "+_ordenCabecera.getFechacreacionorden());	
		OrdenCabecera EntityOrdenCabecera =_ordenCabeceraDao.save(_ordenCabecera);
		return EntityOrdenCabecera;
	}
	
	public OrdenDetalle guardarOrdenDetalle(OrdenDetalle _ordenDetalle) {		
		OrdenDetalle EntityOrdenDetalle= _ordenDetalleDao.save(_ordenDetalle);
		return EntityOrdenDetalle;
	}
	
	public List<OrdenCabecera>  listadoOrden( ) {		
		List<OrdenCabecera> _listaOrden= _ordenCabeceraDao.findAll();
		return _listaOrden;
	}
	
	public  List<OrdenDetalleDtoDao> listadoOrden(int idOrde ) {		
		List<OrdenDetalleDtoDao> _listaOrden= _ordenDetalleDao.consultaordenDetalle(idOrde);
		System.out.println("Total registros eliminados en batch ************************ "+ _listaOrden.get(0).getnombreproducto());	
		/* for(Object ObtjOrdenDetalle:_listaOrden ) {
			
			 lisDteo.setNombreproducto(ObtjOrdenDetalle.getClass().get);
		 }*/
		 
		return _listaOrden;
	}
}
