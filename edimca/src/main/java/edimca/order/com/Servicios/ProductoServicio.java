package edimca.order.com.Servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edimca.order.com.Dao.ProductoDao;
import edimca.order.com.Entity.Producto;

@Service
public class ProductoServicio {
	@Autowired
	private ProductoDao _productoDao;	
	public Producto postProducto(Producto _producto){
		Producto EntyProducto = _productoDao.save(_producto);
		return EntyProducto;
	}
	public List<Producto> allProductos(){
	List<Producto>	lsitProducto = _productoDao.findAll();
		return lsitProducto;
	}
}
