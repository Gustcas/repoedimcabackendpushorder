package edimca.order.com.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import edimca.order.com.Dto.OrdenDetalleDtoDao;
import edimca.order.com.Entity.OrdenCabecera;
import edimca.order.com.Entity.OrdenDetalle;
import edimca.order.com.Servicios.OrdenServicio;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class OrdenControllerRest {
	@Autowired
	private OrdenServicio _ordenServicio;
	
	@PostMapping(value = "/crearOrdenCabecera")
	private ResponseEntity<OrdenCabecera> postOrdenCabecera(@RequestBody OrdenCabecera ordenCabecera) {
		// TODO Auto-generated method stub
		OrdenCabecera _ordenCabecera =_ordenServicio.guardarOrden(ordenCabecera);
		return ResponseEntity.ok(_ordenCabecera);
	}
	
	
	@PostMapping(value = "/crearOrdenDetalle")
	private  ResponseEntity<OrdenDetalle> postOrdenDetalle(@RequestBody OrdenDetalle ordenDetalle) {
		// TODO Auto-generated method stub
		OrdenDetalle _ordenDetalle =	_ordenServicio.guardarOrdenDetalle(ordenDetalle);
		return ResponseEntity.ok(_ordenDetalle);
	}
	
	
	@GetMapping(value = "/listaOrden")
	private ResponseEntity<List<OrdenCabecera>> getOrden() {
		// TODO Auto-generated method stub
		List<OrdenCabecera>  _listadoordenCabecera =_ordenServicio.listadoOrden();
		return ResponseEntity.ok(_listadoordenCabecera);
	}
	
	@GetMapping(value = "/listaOrdenDetalle/{idOrden}")
	private ResponseEntity<List<OrdenDetalleDtoDao>> getOrdenDetalle(@PathVariable("idOrden") int idOrden) {
	// TODO Auto-generated method stub
		List<OrdenDetalleDtoDao>  _listadoordenCabecera =_ordenServicio.listadoOrden(idOrden);
		return ResponseEntity.ok(_listadoordenCabecera);
	}
}
