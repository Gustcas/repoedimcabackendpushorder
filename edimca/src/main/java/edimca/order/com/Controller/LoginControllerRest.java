package edimca.order.com.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import edimca.order.com.Dao.LoginDao;
import edimca.order.com.Dto.UserDto;
import edimca.order.com.Entity.Login;
import edimca.order.com.Servicios.LoginServicio;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })

public class LoginControllerRest {
	@Autowired
	private LoginServicio _loginServicio;

	@GetMapping(value = "/validarLogin/{usurio}/{password}")
	public boolean validarLogin(@PathVariable("usurio") String usurio, @PathVariable("password") String password) {
		boolean isValidarLOgin = _loginServicio.consultaUsuario(usurio, password);
		return isValidarLOgin;
	}

	@PostMapping("login")
	public UserDto login(@Valid @RequestBody UserDto _userDto) {
		System.out.println("llagaste al BE"+  _userDto.getUsername());
		UserDto _isValidarLOgin = _loginServicio.loginUserDto(_userDto.getUsername(), _userDto.getPassword());
		return _isValidarLOgin;
	}

}
