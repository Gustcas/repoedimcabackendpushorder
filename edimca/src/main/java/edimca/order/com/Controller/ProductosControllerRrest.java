package edimca.order.com.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edimca.order.com.Entity.Producto;
import edimca.order.com.Servicios.ProductoServicio;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})

public class ProductosControllerRrest {
	@Autowired
	private ProductoServicio _productoServicio;	
	
	@PostMapping(value = "/crearProducto")
	public ResponseEntity<Producto> crearProducto(@RequestBody Producto producto) {
		Producto EntityProducto = _productoServicio.postProducto(producto);				
		return ResponseEntity.ok(EntityProducto);
	}	
	@GetMapping(value = "/allPorducto")
	public ResponseEntity< List<Producto>> allDataProducto() {
		List<Producto> listProducto = _productoServicio.allProductos();			
		return ResponseEntity.ok(listProducto);
	}	
	
}
