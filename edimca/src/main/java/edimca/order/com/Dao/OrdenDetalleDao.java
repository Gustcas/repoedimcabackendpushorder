package edimca.order.com.Dao;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import edimca.order.com.Dto.OrdenDetalleDtoDao;
import edimca.order.com.Entity.OrdenDetalle;
@Repository
public interface OrdenDetalleDao extends JpaRepository<OrdenDetalle,Integer> {

	@Query(value="select p.nombreproducto,od.cantidad,od.total from ordenDetalle od\r\n" + 
			"inner join ordenCabecera oc on oc.idorden=od.idorden\r\n" + 
			"inner join producto p on p.idproducto=od.idproducto\r\n" + 
			"where oc.estado='A' and od.estado='A' and p.estado='A'\r\n" + 
			"and oc.idorden=?1",nativeQuery = true)
	List<OrdenDetalleDtoDao>consultaordenDetalle(int idOrde);
	
}
