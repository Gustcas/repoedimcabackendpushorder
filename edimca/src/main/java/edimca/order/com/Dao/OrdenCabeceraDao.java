package edimca.order.com.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edimca.order.com.Entity.OrdenCabecera;

@Repository
public interface OrdenCabeceraDao extends JpaRepository<OrdenCabecera,Integer>{

}
