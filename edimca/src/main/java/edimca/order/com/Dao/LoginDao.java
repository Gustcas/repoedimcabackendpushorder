package edimca.order.com.Dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import edimca.order.com.Entity.Login;

import java.util.List;
@Repository
public interface LoginDao extends JpaRepository <Login,Integer>{

	@Query(value  ="select * from login l where l.usuario=?1 and l.password=?2 and l.estado='A'",nativeQuery = true)
	List<Login> isValidoLogin(String usuario,String password);	
	
	@Query(value  ="select * from login l where l.usuario=?1 and l.password=?2 and l.estado='A'",nativeQuery = true)
	Login isValidoLoginUserPass(String usuario,String password);	
}
