package edimca.order.com.Dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import edimca.order.com.Entity.Producto;


@Repository
public interface ProductoDao extends JpaRepository<Producto, Integer>{

}
